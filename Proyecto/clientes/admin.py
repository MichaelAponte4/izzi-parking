from django.contrib import admin
from django.contrib.admin.options import ModelAdmin
from django.db.models.base import Model
from .models import Cliente, Factura, Parqueadero, Puesto, Vehiculo, Reserva
# Register your models here.


admin.site.register(Cliente)
admin.site.register(Factura)
#admin.site.register(Parqueadero)
admin.site.register(Vehiculo)



@admin.register(Parqueadero)
class ParqueaderoAdmin(admin.ModelAdmin):
    list_display = ("placa_vehiculo","ubicacion","hora_puesto","hora_puesto_exit")
    search_fields=("placa_vehiculo","ubicacion")
    
    
@admin.register(Reserva)
class ReservaAdmin(admin.ModelAdmin):
    list_display = ("id","placa","hora_llegada")
    
@admin.register(Puesto)
class PuestoAdmin(admin.ModelAdmin):
    list_display = ("puestos_totales","puestos_llenos", "puestos_vacios")